// 23/U/22570
// ethan lutabi.

#include <stdio.h>
#include <math.h>
int main()
{
    float a,b,c,root1,root2,D,real,imaginary;
    printf("Enter the coefficients a,b and c. \n");
    printf("a: \n");
    scanf("%f",&a);
    printf("b: \n");
    scanf("%f",&b);
    printf("c: \n");
    scanf("%f",&c);
    D = ((b*b)-4*a*c);
    if (D>0)
    {
    root1 = (-b-sqrt(D))/(2*a);
    root2 = (-b+sqrt(D))/(2*a);
    printf("The roots are real and distinct");
    printf("x1 = %.2f\n, x2 = %.2f",root1,root2);
    }
    else if  (D == 0){
    printf("The roots are real and equal.");
    root1 = root2 = -b/(2*a);
    printf("x1 = x2 = %.2f",root1);
    }
    else {
    printf("The roots are complex.");
    real = (-b/(2*a));
    imaginary = sqrt(-D)/(2*a);
    printf("x1 = %.2f+%.2fi\n",real,imaginary);
    printf("x2 = %.2f-%.2fi",real,imaginary);
    }
    return 0;
}
